#!/usr/bin/env bash
#
#            Neuromeka Remote Care Installer Script
#
#   GitLab: https://gitlab.com/daliworks/thingplus-public/neuromeka-remote-care
#   Issues: https://gitlab.com/daliworks/thingplus-public/neuromeka-remote-care/-/issues
#   Requires: bash, mv, rm, tr, type, grep, sed, curl/wget, tar (or unzip on OSX)
#
#   This script installs Neuromeka Remote Care to your path.
#   Usage:
#
#   	$ curl -fsSL https://neuromeka-remote-care.xyz/get.sh | bash
#   	  or
#   	$ wget -qO- https://neuromeka-remote-care.xyz/get.sh | bash
#
#   In automated environments, you may want to run as root.
#   If using curl, we recommend using the -fsSL flags.
#
#   This should work on Mac, Linux, and BSD systems.
#   Please open an issue if
#   you notice any bugs.
#

install_neuromeka_remote_care()
{
	trap 'echo -e "Aborted, error $? in command: $BASH_COMMAND"; trap ERR; return 1' ERR
	neuromeka_remote_care_os="linux"
	neuromeka_remote_care_arch="386"
	install_path="/usr/local/bin"

	# Termux on Android has $PREFIX set which already ends with /usr
	if [[ -n "$ANDROID_ROOT" && -n "$PREFIX" ]]; then
		install_path="$PREFIX/bin"
	fi

	# Fall back to /usr/bin if necessary
	if [[ ! -d $install_path ]]; then
		install_path="/usr/bin"
	fi

	# Not every platform has or needs sudo (https://termux.com/linux.html)
	((EUID)) && [[ -z "$ANDROID_ROOT" ]] && sudo_cmd="sudo"

	#########################
	# Which OS and version? #
	#########################

	neuromeka_remote_care_dir="neuromeka_remote_care"
	neuromeka_remote_care_dl_ext=".tar.gz"

	# NOTE: `uname -m` is more accurate and universal than `arch`
	# See https://en.wikipedia.org/wiki/Uname
	unamem="$(uname -m)"
	case $unamem in
	*86*)
		neuromeka_remote_care_arch="386";;
	*)
		echo "Aborted, unsupported or unknown architecture: $unamem"
		return 2
		;;
	esac

	unameu="$(tr '[:lower:]' '[:upper:]' <<<$(uname))"
	if [[ $unameu == *LINUX* ]]; then
		neuromeka_remote_care_os="linux"
	else
		echo "Aborted, unsupported or unknown OS: $uname"
		return 6
	fi

	########################
	# Download and extract #
	########################

	echo "Downloading neuromeka remote care for $neuromeka_remote_care_os/$neuromeka_remote_care_arch..."
	if type -p curl >/dev/null 2>&1; then
		net_getter="curl -fsSL"
	elif type -p wget >/dev/null 2>&1; then
		net_getter="wget -qO-"
	else
		echo "Aborted, could not find curl or wget"
		return 7
	fi
	
	neuromeka_remote_care_file="neuromeka_remote_care_${neuromeka_remote_care_os}_$neuromeka_remote_care_arch$neuromeka_remote_care_dl_ext"
  neuromeka_remote_care_url="https://gitlab.com/daliworks/thingplus-public/neuromeka-remote-care/-/raw/master/install/$neuromeka_remote_care_file"
  
	echo "$neuromeka_remote_care_url"

	# Use $PREFIX for compatibility with Termux on Android
	rm -rf "$PREFIX/tmp/$neuromeka_remote_care_file"

	${net_getter} "$neuromeka_remote_care_url" > "$PREFIX/tmp/$neuromeka_remote_care_file"

	echo "Extracting..."
	case "$neuromeka_remote_care_file" in
		*.tar.gz) tar -xzf "$PREFIX/tmp/$neuromeka_remote_care_file" -C "$PREFIX/tmp/";;
	esac
	chmod +x "$PREFIX/tmp/$neuromeka_remote_care_dir"

	echo "Putting neuromeka_remote_care in $install_path (may require password)"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/lt" "$install_path/lt"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/filebrowser" "$install_path/filebrowser"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/gotty" "$install_path/gotty"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/auth.sh" "$install_path/auth.sh"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/monitoring" "$install_path/monitoring"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/tpng_streamer" "$install_path/tpng_streamer"
	$sudo_cmd mv "$PREFIX/tmp/$neuromeka_remote_care_dir/tp_version_check.sh" "$install_path/tp_version_check.sh"
	if setcap_cmd=$(PATH+=$PATH:/sbin type -p setcap); then
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/lt"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/filebrowser"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/gotty"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/auth.sh"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/monitoring"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/tpng_streamer"
		$sudo_cmd $setcap_cmd cap_net_bind_service=+ep "$install_path/tp_version_check.sh"
	fi
	$sudo_cmd rm -- "$PREFIX/tmp/$neuromeka_remote_care_file"

	echo "Successfully installed"
	trap ERR
	return 0
}

install_neuromeka_remote_care
