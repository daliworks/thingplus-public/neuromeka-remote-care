# Install Guide

&nbsp;

## 1. Install some libraries

```
$ sudo apt-get update
$ sudo apt-get -y install curl
$ sudo apt-get -y install git
$ sudo apt-get -y install chromium-browser
```

---

&nbsp;

## 2. Check the installation

```
$ curl --version
curl 7.68.0 (x86_64-pc-linux-gnu) libcurl/7.68.0 OpenSSL/1.1.1f zlib/1.2.11 brotli/1.0.7 libidn2/2.2.0 libpsl/0.21.0 (+libidn2/2.2.0) libssh/0.9.3/openssl/zlib nghttp2/1.40.0 librtmp/2.3
Release-Date: 2020-01-08
Protocols: dict file ftp ftps gopher http https imap imaps ldap ldaps pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: AsynchDNS brotli GSS-API HTTP2 HTTPS-proxy IDN IPv6 Kerberos Largefile libz NTLM NTLM_WB PSL SPNEGO SSL TLS-SRP UnixSockets
```

```
$ git --version
git version 2.25.1
```

## 3. Execute shell script for installation

```
$ curl -sL https://gitlab.com/daliworks/thingplus-public/neuromeka-remote-care/-/raw/master/get.sh
```

- If it's already installed, then execute below command and check the version
- If version is not latest version, it'll install latest version

```
$ sudo bash tp_version_check.sh
```

---

&nbsp;

## 4. Execute domains and services

- ### File-Domain

```
$ lt --port 20080 --host http://lt.thingbine.com &
```

- ### Terminal-Domain

```
$ lt --port 29090 --host http://lt.thingbine.com &
```

- ### Cam-Domain

```
$ lt --port 23000 --host http://lt.thingbine.com &
```

<br>

- ### File-Service

```
$ filebrowser -a 127.0.0.1 -p 20080 -r / &
```

- ### Terminal-Service

```
$ gotty --permit-write --reconnect -p 29090 /usr/local/bin/auth.sh &
```

- ### Cam-Service

```
$ tpng_streamer --port 23000 &
$ monitoring --port 23000 --id ${CAM_DOMAIN_NAME} &
```

- If CAM_DOMAIN_URL is https://black-bird-35.lt.thingbine.com, then CAM_DOMAIN_NAME should be black-bird-35

---

&nbsp;

## 5. Close Service

- If you want to disallow remote control, then kill Service not Domain

```
kill -2 PID
```

---
